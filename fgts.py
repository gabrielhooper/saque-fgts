#!/usr/bin/env python
# coding: utf-8
import numpy as np
import datetime
def fgts_14(salario,saldo,mes):

    a = 'aliquota'
    b = 'parcela'
    tabela_fgts = {
        500:{a:0.5,b:0},
        1000:{a:0.4,b:50},
        5000:{a:0.3,b:150},
        10000:{a:0.3,b:650},
        15000:{a:0.15,b:1150},
        20000:{a:0.1,b:1900},
        9999999:{a:0.05,b:2900}
    }

    rendimento_anual_fgts = 1.03
    fgts_mensal = 0.08

    rendimento_mensal_fgts = np.power(rendimento_anual_fgts,(1/12))

    def saldo_retirado(saldo_total):
        for key in tabela_fgts.keys():
            if saldo_total<=key:
                return saldo_total*tabela_fgts[key][a] + tabela_fgts[key][b]
            
    def ano_mes_14(salario,saldo,mes_niver):
        ano_agora = datetime.datetime.now().year
        mes_agora = datetime.datetime.now().month
        valor_retirado = []
        saldo_total = saldo
        for ano in range(21):
            if ano==0: ##Retorna quantidade de meses até o próximo pagamento. necessário apenas no primeiro ano, após isso são sempre 12 meses
                if ano_agora==2019 or mes_agora>mes_niver: #Pagamentos iniciam somente em 2020
                    range_mes = 12 - mes_agora + mes_niver
                else:
                    range_mes = mes_niver-mes_agora
            else:
                range_mes = 12
            for mes in range(range_mes):
                saldo_total *= rendimento_mensal_fgts
                saldo_total += salario*fgts_mensal
            if ano==0 and ano_agora==2019: ##Não haverá retiradas em 2019
                continue
            valor_retirado.append("<strong>" + str(ano_agora+ano+1) +"</strong>: R$"+ str(int(saldo_retirado(saldo_total))))
            saldo_total -= saldo_retirado(saldo_total)
            
        return valor_retirado
            
    return ",".join(ano_mes_14(salario,saldo,mes))




