# saque-fgts

Descubra o quanto irá receber no saque aniversário do FGTS http://gabrielhooper.pythonanywhere.com/ (Link válido somente até 03/2020)

Tela inicial:

![alt text](https://gitlab.com/gabrielhooper/saque-fgts/raw/master/resultado/P-A-FGTS.png "Tela inicial")


Tela de resultados:

![alt text](https://gitlab.com/gabrielhooper/saque-fgts/raw/master/resultado/FGTS.png "Exemplo de resultado")
