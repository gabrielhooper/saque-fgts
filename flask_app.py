
# A very simple Flask Hello World app for you to get started with...

from flask import Flask,render_template,request
import numpy as np
import fgts

app = Flask(__name__,template_folder='/home/gabrielhooper/mysite/src')

@app.route('/')
def hello_world():
    return render_template('/index.html') #str(os.system('pwd'))#

@app.route('/calculo')
def hello_world2():
    salario = int(request.args.get('slr',default = '2000'))
    saldo = int(request.args.get('sdo',default = '0'))
    mes = int(request.args.get('mes',default = '1'))
    return fgts.fgts_14(salario,saldo,mes)