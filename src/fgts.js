$(document).ready(()=>{
    $("input[required], select[required]").attr("oninvalid", "this.setCustomValidity('Requirido')");
    $("input[required], select[required]").attr("oninput", "setCustomValidity('')");
    $('[data-toggle="tooltip"]').tooltip();
})

function envia_form(){
    
    var isvalid = $('#formulario')[0].checkValidity()
    
    if (isvalid){
        var campos = $('#formulario').serialize()
        $.get('http://gabrielhooper.pythonanywhere.com/calculo?'+campos)
            .done(function(resultado){
                $('#tabela-body').empty()
                for(res of resultado.split(',')){
                    var ano = res.split(':')[0]
                    var valor = res.split(':')[1]
                    var linha = '<tr scope="row"><td>'+ano+'</td><td>'+valor+'</td></tr>'
                    $('#tabela-body').append(linha)
                }
                //$('#valor')[0].innerHTML = resultado
                $('#resultado').show()
            })
    }
    else{
        $('#formulario')[0].reportValidity()
    }
    
}

